.. _examples:

Examples
========

The :ref:`quick examples <quick_examples>` are all self-contained and can be
copy-pasted to a source file or Python console to reproduce them. Most of the
remaining examples have a tutorial character and are presented in multiple
dependent code sections.

.. toctree::
   :maxdepth: 2

   quick
   graphs
   complex
   qrep
   optdes
   constraints
